library(shiny)
library(shinyjs)
library(shinythemes)
library(shinydashboard)
library(htmltools)
ui = navbarPage(htmlOutput("title"),
             windowTitle = 'Analytical Dashboard',
  

  theme=shinytheme('simplex'),

  tabPanel(HTML(paste0(icon('file-upload'),' Load data')),
           tags$style(type='text/css',
                      '.well {background-color: #edf4ff; border: 1px solid #999999;}'),
           tags$style(type='text/css',
                      'body { color: #1c1c1c; }'),
           
    sidebarLayout(
      sidebarPanel(width=3,
        
          # Input: Select a file ----
          fileInput("file1", "Choose new CSV or TXT file",
                    multiple = TRUE,
                    accept = c("text/csv",
                               "text/comma-separated-values,text/plain",
                               ".csv")),
          
          tags$hr(),
          
          # HEADER
          checkboxInput("header", "Header", TRUE),
          
          # SEPARATOR
          radioButtons("sep", "Separator",
                       choices = c(Comma = ",",
                                   Semicolon = ";",
                                   Tab = "\t",
                                   Space = " ",
                                   Colon = ":"),
                       selected = ","),
          
          tags$hr(),
          tags$hr(),
          downloadButton(outputId = 'file',
                         label = 'Save dataset')
          # ,downloadButton(outputId = 'report',
          #                label = 'Report for current GLM' )
          
      ),
      mainPanel(width=9,
        align='center',
        htmlOutput('msg_data'),
        div(HTML('<font size="-2"><em>Top six rows</em></font>'),style = 'overflow-x: auto',  tableOutput("contents")),
        
        tags$style(HTML('.info-box {min-height: 60px;} .info-box-icon {height: 60px; line-height: 60px;} .info-box-content {padding-top: 1px; padding-bottom: 1px;}')),
        tags$br(),tags$br(),
        fluidRow(
          column(width=4,
                 infoBoxOutput("box_na",width = 12)
                 ),
          column(width=4,
                 infoBoxOutput("box_nrow",width = 12)
          ),
          column(width=4,
                 infoBoxOutput("box_ncol",width = 12)
          )
        )
      )
    )
  ),
  
  
  tabPanel(HTML(paste0(icon('chart-bar'),' Summary')),
    sidebarLayout(
      sidebarPanel(width=3,
        checkboxGroupInput(inputId = 'fact',
                           label = 'Change to factor/numeric variable:',
                           choices = NULL,
                           selected = NULL)
      ),
      mainPanel(
        uiOutput("summary")
      )
    )
  ),
  
  tabPanel(HTML(paste0(icon("chart-scatter"),'Plots')),
           sidebarLayout(
              sidebarPanel(width=3,
                     selectInput(inputId = 'x',
                                 label = 'Scatter x-axis',
                                 choices = NULL, selected = NULL),
                     selectInput(inputId = 'y',
                                 label = 'Scatter y-axis',
                                 choices = NULL, selected = NULL),
                     selectInput(inputId = 'z',
                                 label = 'Scatter color by:',
                                 choices = NULL, selected = NULL),
                     tags$hr(),
                     textOutput('msg_dropped'),
                     tags$hr(),
                     selectInput(inputId = 'ctype', label = 'Correlation type',
                                 choices = c("Pearson", "Spearman"),
                                 selected = 'Pearson')
              ),
              mainPanel(
                plotOutput("plot"), 
                br(), br(),
                plotOutput('cor'),
                br(), br(),
                plotOutput('miss'), br(),
                sliderInput(inputId = 'size',
                            label = 'Label size of Missing plot. Adjust if not visible',
                            min = 0.1, max = 1.5, value = 1, step = 0.1), br(), value = 2,
                            align = "center"
              )
           )
           ),
  tabPanel('GLM',
    sidebarLayout(
      sidebarPanel(width=3,
                   selectInput(inputId = 'model', 
                   label = 'Family',
                   choices = c('Binomial' = 'binomial'),
                   selected = 'binomial'),
                  selectInput(inputId = 'response', label= 'Response', 
                              choices = NULL, selected = NULL),
                  checkboxGroupInput(inputId = 'vars',
                                     label = 'Independent variables:',
                                     choices = NULL,
                                     selected = NULL
                  ),
                  actionButton(inputId = 'go3', label = 'Predict',icon=icon('search')),
                  checkboxInput(inputId = 'probs',
                                label = 'Plot estimates impact using probabilities',
                                value = F)

      ),
      mainPanel(
        align = 'center',
                 
                 textOutput('msg_glm'), textOutput('msg_glm2'),
                 tableOutput('glmsum2'), tableOutput('glmsum'),
                 plotOutput('impact'),
                 br(),
                 textOutput('sav_msg'),

                                   # saving loading glm buttons
                                   div(style="display:inline-block",
                                       actionButton('save_mod1', label = 'Save model #1',
                                                    style="color: #3E3F3A; background-color: #ccd6e8;
                                                    background-image: none;"),
                                       actionButton('load_mod1', label = 'Load model #1',
                                                    style="color: #3E3F3A; background-color: #ccd6e8;
                                                    background-image: none;")),
                                   div(style="display:inline-block",
                                       actionButton('save_mod2', label = 'Save model #2',
                                                    style="color: #3E3F3A; background-color: #ccd6e8;
                                                    background-image: none;"),
                                       actionButton('load_mod2', label = 'Load model #2',
                                                    style="color: #3E3F3A; background-color: #ccd6e8;
                                                    background-image: none;")),
                                   tags$hr(),
                                   actionButton('anova',
                                                label = 'Anova for saved models #1:#2',
                                                style="color: #3E3F3A; background-color: #dce2ed;
                                                background-image: none;"),
                                   tableOutput('anova')
      )
    )
  ),
  tabPanel('Clustering',
           sidebarLayout(
             sidebarPanel(width=3,
               
                         selectInput(
                           inputId = 'clust_type',
                           label = 'Clustering method',
                           choices = c(
                             'K-means Hartigan–Wong ' = 'kmeans',
                             'Fast K-medoids' ='kmed'
                           ),
                           selected = 'kmeans'
                         ),
                         selectInput(
                           inputId = 'dist_clust',
                           label = 'Metric (k-medoids)',
                           choices = c(
                             'Euclidean'  = 'euclidean',
                             'Manhattan' = 'manhattan'
                           )
                         ),
                         numericInput(
                           inputId = 'nclust',
                           label = 'Number of clusters',
                           min = 2,
                           max = 10,
                           value = 2,
                           step = 1
                         ),
                         numericInput(
                           inputId = 'nstart',
                           label = 'Number of starting sets',
                           min = 1,
                           max = 100,
                           value = 5,
                           step = 1
                         ),
                         actionButton(
                           inputId = 'go_clust',
                           label = 'Go'
                         ),
                         actionButton(
                           inputId = 'add_clust',
                           label = 'Append clustered var',
                           icon=icon('plus-circle')
                         ),
                         br(),br(),
                         checkboxGroupInput(
                           inputId = 'clust_in',
                           label = 'Variables to cluster',
                           choices = NULL,
                           selected = NULL
                         )

             ),
             mainPanel(
                         align = 'center',
                         textOutput('warn_clust'),
                         textOutput('warn_clust2'),
                         tableOutput('summ_clust'),
                         tableOutput('tots_clust'),
                         plotOutput('clust_plot1'),
                         selectInput(inputId = 'xclust',
                                    label = 'Scatter x-axis',
                                    choices = NULL, selected = NULL),
                         selectInput(inputId = 'yclust',
                                     label = 'Scatter y-axis',
                                     choices = NULL, selected = NULL),
                         plotOutput('clust_plot2')
             )
           )
           ),
  tabPanel('Variable Modifications',
           tags$style(HTML('.help-block {color: inherit;}')),
           sidebarLayout(
             sidebarPanel(width=3,
               helpText("To create a new variable you can use standard R syntax that can be evaluated."),
               helpText("Example 1) Name: X1; Definition: SibSp + Parch will create a new variable with a name X1."),
               helpText("Example 2) Name blank; Definition: exp(Income) will create a new variable with a default name."),
               helpText("Example 3) interaction(Gender, Education) will create an interaction factor variable."),
               helpText("Example 4) cut(Sepal.Width, c(2,3,5)) divides numeric variable to interval (2,3] and (3,5] and converts to factor")

             ),
             mainPanel(
                         align = 'center',
                         # ADD VARS
                                           fluidRow(
                                             textOutput('var_msg2'),
                                             br(),
                                             div(style="display:inline-block",
                                                 textInput(inputId = 'add_var_name',
                                                           label = 'Variable name: ',
                                                           value = NULL, width = '70%')),
                                             div(style="display:inline-block",
                                                 textInput(inputId = 'add_var',
                                                           label = 'Definition: ',
                                                           value = NULL)),
                                             div(style="display:inline-block",
                                                 actionButton(inputId = 'go_var', icon=icon('plus-circle'),
                                                              label = 'Create'))),
                                           # REMOVE VARS
                                           tags$hr(),
                                           fluidRow(
                                             textOutput('var_msg'),
                                             selectInput(inputId = 'rm_var',
                                                                                          label = 'Variable to be removed. Cannot be undone.',
                                                                                          choices = NULL,
                                                                                          selected = NULL),
                                             actionButton(inputId = 'go_var2', icon = icon('trash'),
                                                                                           label = 'Delete')

                                           ))
             )
           ),
  tabPanel('Random Forest',
    sidebarLayout(
      sidebarPanel(width=3,
        sliderInput(inputId = 'ntree',
                    label = 'Number of trees:',
                    min=2, max=1000, step=1, value=100),
                    actionButton('go',"Predict",
                                 icon=icon('search')), 
                    tags$hr(),
        selectInput(inputId = 'response_rf', label= 'Response', 
                    choices = NULL, selected = NULL),
                    checkboxGroupInput(inputId = 'vars_rf',
                    label = 'Independent variables:',
                    choices = NULL,
                    selected = NULL),
        numericInput(inputId = 'seed', label = 'Set seed', 
                     min = 1, value = 10)
      ),
      mainPanel(
        align='center',
        textOutput('check'),
        htmlOutput('msg'), textOutput('warn_rf'),
        br(), plotOutput('plotRF'), 
        plotOutput('RFimp')
      )
    )
  ),
  
  tabPanel('ROC',
           sidebarLayout(
             sidebarPanel(width=3,
               
                       numericInput(inputId = 'Cutoff',
                                    label = 'Cutoff [0,1]',
                                    value = 0.5,
                                    min = 0,
                                    max = 1,
                                    step = 0.01),
                       numericInput(inputId = 'Fbeta',
                                    label = 'F1 Score parameter',
                                    value = 1,
                                    min = 0,
                                    max = 1000,
                                    step = 0.1),
                       numericInput(inputId = 'FPcost', label = 'Cost of False Positive',
                                    value = 1, min = -10, max = 10),
                       numericInput(inputId = 'FNcost', label = 'Cost of False Negative',
                                    value = 1, min = -10, max = 10)
               
             ),
             mainPanel(width=9,
               align = 'center',
               textOutput('msg_roc'), textOutput('msg_roc2'),
               htmlOutput('cutoff_msg'),
               tableOutput({'Best_cutoff'}),
               tableOutput('AUC'), 
               fluidRow(
                      column(tableOutput("conf_matrix"), width = 4, offset = 2),
                      column(tableOutput("conf_matrix_rf"), width = 4)),
               plotOutput('ROC'),
               plotOutput('Cost')
             )
           )
           )
  ,
  navbarMenu('Others',
    tabPanel('Residuals',
               sidebarLayout(
                 sidebarPanel(
                   checkboxInput(
                               inputId = 'scat_res',
                               label = 'Switch to scatter plots'
                             ),
                             checkboxInput(
                               inputId = 'deviance',
                               label = 'Deviance instead of residuals'
                             ),
                             checkboxInput(
                               inputId = 'std_glm_plots',
                               label = 'Show standard glm plots'
                             )
                 ),
                 mainPanel(align='center',
                   div(plotOutput('resid', inline = T), 
                                                  tableOutput('err_resid'), br(),
                                                  plotOutput('predicted_glm', inline = T),
                                                  tableOutput("err_glm")),
                                                  plotOutput('qq'),
                                                  plotOutput('std_glm_plot1'),
                                                  plotOutput('std_glm_plot3'),
                                                  plotOutput('std_glm_plot5')
                 )
               )
               )
    ,
    tabPanel('Dist. Fitting',
             sidebarLayout(
               sidebarPanel(width=3,
                          selectInput(
                             inputId = 'var_test',
                             label = 'Choose variable to perform tests: ',
                             choices = NULL,
                             selected = NULL
                           ),
                           selectInput(
                             inputId = 'method',
                             label = 'Estimation method: ',
                             choices = c('MLE' = 'mle'),
                             selected = 'mle')
               ),
               mainPanel(tableOutput('tests2'), 
                         plotOutput('thist'), 
                         tableOutput('tests'),
                         align='center',
                         width=9),
             )
             )
    ,
    tabPanel('Rpart',
             sidebarLayout(
               sidebarPanel(wisth=4,
                                     sliderInput(inputId = 'mdepth', label = 'Max. allowed depth of a tree:',
                                                 min = 2, max = 30, value = 30),
                                     sliderInput(inputId = 'msplit', label = 'Min. observations in a node to split:',
                                                 min = 2, max = 100, value = 20),
                                     numericInput(inputId = 'cp', label = 'Complexity parameter [-1,1]',
                                                  min = -1, max = 1, value = 0.001, step = 0.01 ),
                                     checkboxInput(inputId = 'rrpois',
                                                   label = 'Poisson regression',
                                                   value = FALSE
                                     ),
                                     actionButton('go2',"Predict",icon=icon('search')),
                                     tags$hr(),
                                     selectInput(inputId = 'response_rpart', label= 'Response',
                                                 choices = NULL, selected = NULL),
                                     checkboxGroupInput(inputId = 'vars_rpart',
                                                        label = 'Independent variables:',
                                                        choices = NULL,
                                                        selected = NULL
                                                         ),
                                      numericInput(inputId = 'seed2', label = 'Set seed',
                                                   min = 1, value = 10)
               ),
               mainPanel(
                 textOutput('msg_rpart'),
                 plotOutput('rplot'), tableOutput('rtbl'), plotOutput('rplot2'),
                 align='center',
                 width=8
               )
             ))
    ,
    tabPanel('About',
             sidebarLayout(
               
               sidebarPanel=NULL,
               mainPanel(width=9,
                         useShinyjs(),
               shinydashboard::box(textOutput('about_text')),
                 shinydashboard::box(title='Global Options', collapsible = T,
                                     sliderInput('font_size','Font size',min = 5, max = 25,
                                                 value = 13,step = 1))

               )
             ))
    ,
    tabPanel('Dataset',
             sidebarLayout(
               sidebarPanel(width=2,
                 numericInput('nrw', 'Number of rows', min=1, 
                              value = 3, max = max())
               ),
               mainPanel(width=10,
                         div(style = 'overflow-x: auto',  dataTableOutput('table'))
               )
             ))
  )
)


addDeps <- function(x) {
  if (getOption("shiny.minified", TRUE)) {
    adminLTE_js <- "app.min.js"
    shinyjqui_js = 'shinyjqui.min.js'
    adminLTE_css <- c("AdminLTE.min.css", "_all-skins.min.css")
  } else {
    adminLTE_js <- "app.js"
    shinyjqui_js = 'shinyjqui.js'
    adminLTE_css <- c("AdminLTE.css", "_all-skins.css")
  }
  
  dashboardDeps <- list(
    htmlDependency("AdminLTE", "2.0.6",
                   c(file = system.file("AdminLTE", package = "shinydashboard")),
                   script = adminLTE_js,
                   stylesheet = adminLTE_css
    ),
    htmlDependency("shinydashboard",
                   as.character(utils::packageVersion("shinydashboard")),
                   c(file = getwd()),
                   script = "shinydashboard.js",
                   stylesheet = "shinydashboard.css")

  )
  
  
  shinydashboard:::appendDependencies(x, dashboardDeps)
}


ui <- addDeps(
  tags$body(shiny::fluidPage(ui)
  )
)

